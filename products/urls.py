from django.urls import path
from .views import edit_view, favorite_view, create_view, delete_view, list_view


urlpatterns = [

    path("create", create_view, name="create_product"),
    path("edit/<int:product_id>", edit_view, name="edit_product"),
    path("delete/<int:product_id>", delete_view, name="delete_product"),
    path("list-my-products", list_view, name="list_my_products"),
    path("my-favorites", favorite_view, name="my_favorites"),


]