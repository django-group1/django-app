from django.db import models
from stores.models import Store
from datetime import datetime


COLOR_CHOICES = [('1','Blue'),('2','Red'),('3','White'),('4', 'Black'),]

class Product(models.Model):
    name = models.CharField(max_length=150)
    color = models.CharField(max_length=50, choices=COLOR_CHOICES)
    price = models.PositiveSmallIntegerField()
    favorite = models.ManyToManyField('users.CustomUser', blank=True, related_name="product_favorite")
    picture = models.ImageField(default='/product2.jpg', upload_to=datetime.now().strftime('%Y%m%d%H%M%S%f'))
    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
