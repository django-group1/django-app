from django import forms
from .models import Product, COLOR_CHOICES
from stores.models import Store


class ProductForm(forms.ModelForm):
    
    picture = forms.ImageField(label='Product Image', required=False, error_messages = {'invalid':"Image files only"}, widget=forms.FileInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['store'].queryset  = Store.objects.filter(owner=self.user)

    class Meta:
        model = Product
        fields = ("name", "color", "price", "picture", "store",)

