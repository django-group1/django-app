from django import template
from django.contrib.auth.models import User
from products.models import Product
from users.models import CustomUser

register = template.Library()
@register.filter
def is_product_favorited(user, product_id):
    product = Product.objects.get(id=product_id)

    for fav_user in product.favorite.all():
        if fav_user == user:
            return True
    return False