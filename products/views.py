from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from stores.models import Store
from .models import Product
from .forms import ProductForm
from django.core.exceptions import ObjectDoesNotExist


@login_required(login_url="/login/")
def index(request):
    all_products = Product.objects.all().order_by('-id')
    return render(request, "index.html", {"products":all_products})
    

@login_required(login_url="/login/")
def list_view(request):
    my_stores = Store.objects.filter(owner=request.user)
    products = Product.objects.all()
    arr = []
    for i in products:
        if i.store.owner == request.user:
            arr.append(i)
    
    return render(request, "product/my_products.html", {"owned_products":arr})


@login_required(login_url="/login/")
def create_view(request):
    if request.method == "POST":
        form = ProductForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect("index")
    else:
        form = ProductForm(request.user)
        stores = Store.objects.filter(owner=request.user)
    return render(request, "product/create_product.html", {"form":form, "stores":stores})


@login_required(login_url="/login/")
def edit_view(request, product_id):
    product = Product.objects.get(id=product_id)

    if request.method == "POST":
        form = ProductForm(request.user, request.POST, request.FILES, instance=product)
        
        if form.is_valid():
            form.save()
            return redirect("index")
        else:
            e1 = form.errors
            e2 = form.non_field_errors
            return render(request, "product/edit_product.html", {"error":form.errors})
    else:
        form = ProductForm(request.user, instance=product)
        return render(request, "product/edit_product.html", {"form":form})


@login_required(login_url="/login/")
def delete_view(request, product_id):
    product = Product.objects.get(id=product_id)
    product.delete()
    return redirect("index")
    

# could have implemented in utils.py or some kind of similar file
def get_or_none(classmodel, param):
    try:
        return classmodel.objects.filter(favorite=param)
    except classmodel.DoesNotExist:
        return None


@login_required(login_url="/login/")
def favorite_view(request):
    user = request.user
    favorited_products = get_or_none(Product, user)
    favorited_stores = get_or_none(Store, user)
    return render(request, "all_favorites.html", {"products":favorited_products, "stores":favorited_stores})
