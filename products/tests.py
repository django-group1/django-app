from django.test import TestCase
from django.contrib.auth.models import User
from django.http import HttpRequest
from django.test import Client, RequestFactory, TestCase
from django.urls import reverse

from stores.models import Store
from products.models import Product
from products.views import *
from users.models import CustomUser


# Create your tests here.
class TestViewResponses(TestCase):
    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()
        self.user = CustomUser.objects.create(email='admin@abc.com')
        self.store = Store.objects.create(name='teststore', city='mycity', owner = self.user)
        Product.objects.create(name='django',price=12, store=self.store )
        Product.objects.create(name='django-2',price=1232, store=self.store )

    def test_homepage_url(self):
        """
        Test homepage response status
        """
        url = reverse('index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_product_list_url(self):
        """
        Test category response status
        """
        url = reverse('list_my_products')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


