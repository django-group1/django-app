# Django Store App

This app allows users to create products and stores, which they can add them to their favorites. You can sign-up for a new account or log-in using these credentials:

Owner:
   email: testowner@test.com
   password: Fazlagida21
Customer:
   email: testcustomer@test.com
   password: Fazlagida21

# API documentation

This part contains the documentation for Django app API.

#### Contents

- [Overview](#1-overview)
- [Authentication](#2-authentication)
- [User-Favorite Actions](#3-user-favorite-actions)
  - [Products](#31-products)
  - [Stores](#32-stores)
- [Testing](#4-testing)

## 1. Overview

This app’s API is using atoken based authentication. All requests are made to endpoints beginning:
`127.0.0.1/api`.

## 2. Authentication

In order to send a secure request, user need to login via `127.0.0.1/api-token-auth/` with sending `username` and `password` in body as JSON. Then user has been verified with a given token in the response.

With the following parameters:

| Parameter       | Type     | Required?  | Description                                     |
| -------------   |----------|------------|-------------------------------------------------|
| `username`      | string   | required   | The email of the user.                          |
| `password`      | string   | required   | Password.                                       |

If successful, you will receive back an access token response with 200 OK response:

```
{
    Token: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
}
```  

###  3. User-Favorite Actions
There are 2 endpoints for favorite actions, for product and store. 

###  3.1 Products
In order to favorite/unfavorite a product, user must send POST/DELETE request to `127.0.0.1/api/favorite-product`. Only product ID is required in body as JSON format.

For adding a product to user's favorite, product ID must be given in body as JSON format like below.

| Parameter       | Type     | Required?  | Description                                     |
| -------------   |----------|------------|-------------------------------------------------|
| `id`            | string   | required   | The ID of the product.                          |

If successful, you will receive back the response below with 200 OK response:

```
{
   'Added to favorite': product name, 'for:': user email.
}
```  
For un-favorite a product, user must send DELETE request to the same domain with product ID in body as JSON format.
| Parameter       | Type     | Required?  | Description                                     |
| -------------   |----------|------------|-------------------------------------------------|
| `id`            | string   | required   | The ID of the product.                          |

If successful, you will receive back the response below with 200 OK response:

```
{
   'removed from favorites'
}
```

### 3.2 Stores
For favorite/unfavorite store, it is the same steps overall. Endpoint address is `127.0.0.1/api/favorite-store` for store favorite actions.

| Parameter       | Type     | Required?  | Description                                     |
| -------------   |----------|------------|-------------------------------------------------|
| `id`            | string   | required   | The ID of the store.                          |

If successful, you will receive back the response below with 200 OK response:

```
{
   'Added to favorite': store name, 'for:': user email.
}
```  
For un-favorite a store, user must send DELETE request to the same domain with store ID in body as JSON format.
| Parameter       | Type     | Required?  | Description                                     |
| -------------   |----------|------------|-------------------------------------------------|
| `id`            | string   | required   | The ID of the store.                            |

If successful, you will receive back the response below with 200 OK response:

```
{
   'removed from favorites'
}
```

## 4. Testing

If you want to run testing in Django you must go to project directory and run `python manage.py test [app_name]`. In this project, app names can be seen as `api`, `products`, `stores` and `users`. 

Hovewer not all tests are completed in this project yet.