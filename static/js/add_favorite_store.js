function addFavoriteStore(button, store_id) {

    if (button.name == "add_favorite") {

        var button_css = button;
        $.ajax({
            type: "POST",
            url: "/api/favorite-store",
            headers: {
                'X-CSRFToken': csrftoken
            },
            data: {
                id: parseInt(store_id),
            },

            success: function(result) {
                button.style.color = "#F4B30A";
                button.name = "remove_favorite";
                alert("Added to favorites!");
            },
            error: function(result) {
                alert('error');
            }
        });

    } else if (button.name == "remove_favorite") {

        var button_css = button;
        $.ajax({
            type: "DELETE",
            url: "/api/favorite-store",
            headers: {
                'X-CSRFToken': csrftoken
            },
            data: {
                id: parseInt(store_id),
            },
            success: function(result) {
                button.style.color = "#FFFFFF";
                button.name = "add_favorite";
                alert("Removed from favorites!");
            },
            error: function(result) {
                alert('error');
            }
        });

    }
};