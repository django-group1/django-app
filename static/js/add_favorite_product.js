function addFavoriteProduct(button, product_id) {
    var ppp = button.name;
    if (button.name == "add_favorite") {

        var button_css = button;
        $.ajax({
            type: "POST",
            url: "/api/favorite-product",
            headers: {
                'X-CSRFToken': csrftoken
            },
            data: {
                id: parseInt(product_id),
            },

            success: function(result) {
                button.style.color = "#F4B30A";
                button.name = "remove_favorite";
                alert("Added to favorites!");
            },
            error: function(result) {
                alert('error');
            }
        });

    } else if (button.name == "remove_favorite") {

        var button_css = button;
        $.ajax({
            type: "DELETE",
            url: "/api/favorite-product",
            headers: {
                'X-CSRFToken': csrftoken
            },
            data: {
                id: parseInt(product_id),
            },
            success: function(result) {
                button.style.color = "#FFFFFF";
                button.name = "add_favorite";
                alert("Removed from favorites!");
            },
            error: function(result) {
                alert('error');
            }
        });

    }
};