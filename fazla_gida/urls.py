from django.urls import path, include
from users.views import signup
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.urls import path, include
from users.views import login_view
from products.views import index
from rest_framework.authtoken import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [

    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
    path('product/', include('products.urls')),
    path('store/', include('stores.urls')),
    path('signup', signup, name='signup'),
    path('login/', login_view , name ='login'),
    path('logout/', auth_views.LogoutView.as_view(), name ='logout'),
    path('', index, name="index"),
    path('api-token-auth/', views.obtain_auth_token, name='api-token-auth'),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)