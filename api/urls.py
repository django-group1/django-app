from django.urls import path
from .views import FavoriteProduct, FavoriteStore


urlpatterns = [

    path('favorite-product', FavoriteProduct.as_view(), name="add-favorite-product"),
    path('favorite-store', FavoriteStore.as_view(), name="add-favorite-store"),
    
]