from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory
from rest_framework.test import APITestCase
from rest_framework.test import force_authenticate
from products.models import Product
from stores.models import Store
from users.models import CustomUser
from rest_framework.test import APIClient
from django.urls import reverse


class EndpointViewTest(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.factory = APIRequestFactory()
        self.user = CustomUser.objects.create_user(email='user@foo.com', password='top_secret')
        self.token = Token.objects.create(user=self.user)
        self.token.save()
        self.store = Store.objects.create(name="store_test", city="Abc", owner=self.user)
        self.product = Product.objects.create(name="product_test", price=1000, color=1,store=self.store)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)



    def test_add_favorite_product(self):
        url = reverse('add-favorite-product')
        data = {'id':self.product.id}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_remove_favorite_product(self):
        url = reverse('add-favorite-product')
        data = {'id':self.product.id}
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_add_favorite_store(self):
        url = reverse('add-favorite-store')
        data = {'id':self.store.id}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 200)
    
    def test_remove_favorite_store(self):
        url = reverse('add-favorite-store')
        data = {'id':self.store.id}
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, 200)