from rest_framework.response import Response
from rest_framework import generics, status
from products.models import Product
from stores.models import Store
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from .serializers import ProductSerializer, StoreSerializer


class FavoriteProduct(generics.GenericAPIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    serializer_class = ProductSerializer
    
    
    def get_queryset(self):
        return Product.objects.get(id=self.request.data['id'])

    def post(self, request, **kwargs):
        user = request.user
        product = self.get_queryset()
        product.favorite.add(user)
        product.save()
        return Response({'Added to favorite': product.name, 'for:': user.email }, status=status.HTTP_200_OK)

    #remove from favorites
    def delete(self, request, **kwargs):
        product = self.get_queryset()
        product.favorite.remove(request.user)
        return Response({'removed from favorites'}, status=status.HTTP_200_OK)


class FavoriteStore(generics.GenericAPIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    serializer_class = StoreSerializer
    
    
    def get_queryset(self):
        return Store.objects.get(id=self.request.data['id'])

    def post(self, request, **kwargs):
        user = request.user
        store = self.get_queryset()
        store.favorite.add(user)
        store.save()
        return Response({'Added to favorite': store.name, 'for:': user.email }, status=status.HTTP_200_OK)

    # remove from favorites
    def delete(self, request, **kwargs):
        store = self.get_queryset()
        store.favorite.remove(request.user)
        return Response({'removed from favorites'}, status=status.HTTP_200_OK)

