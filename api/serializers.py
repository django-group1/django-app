from rest_framework import serializers
from users.models import CustomUser
from products.models import Product
from stores.models import Store


class UserSerializer(serializers.ModelSerializer): #did nnot used
    class Meta:
        model = CustomUser
        fields = ["email"]


class ProductSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Product
        fields = ["id"]


class StoreSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Store
        fields = ["id"]