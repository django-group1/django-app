from django.db import models
from users.models import CustomUser


# Create your models here.
class Store(models.Model):

    name = models.CharField(max_length=250)
    city = models.CharField(max_length=50)
    favorite = models.ManyToManyField(CustomUser,blank=True, related_name="store_favorite")
    owner = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
