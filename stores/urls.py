from django.urls import path

from products.views import delete_view
from .views import create_store, list_my_stores, edit_view, delete_view, list_view


urlpatterns = [

    path("create", create_store, name="create_store"),
    path("list", list_view, name="list_stores"),
    path("list-my-stores", list_my_stores, name="list_my_stores"),
    path("edit/<int:store_id>", edit_view, name="edit_store"),
    path("delete/<int:store_id>", delete_view, name="delete_store"),

]