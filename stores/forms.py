from django import forms
from django.forms import fields
from .models import Store

class StoreForm(forms.ModelForm):

    class Meta:
        model = Store
        fields = ("name", "city",)