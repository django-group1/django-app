from django import template
from django.contrib.auth.models import User
from stores.models import Store

register = template.Library()
@register.filter
def is_store_favorited(user, store_id):
    store = Store.objects.get(id=store_id)

    for fav_user in store.favorite.all():
        if fav_user == user:
            return True
    return False