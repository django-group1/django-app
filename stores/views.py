from django.shortcuts import redirect, render
from stores.models import Store
from .forms import StoreForm
from django.contrib.auth.decorators import login_required


@login_required(login_url="/login/")
def list_view(request):
    s = Store.objects.all()
    return render(request, "store/list_stores.html", {"stores":Store.objects.all()})


@login_required(login_url="/login/")
def create_store(request):
    
    if request.method == "POST":
        form = StoreForm(request.POST)
        if form.is_valid():
            new_store = form.save(commit=False)
            new_store.owner = request.user
            new_store.save()
            return redirect("index")
    else:
        form = StoreForm()
        return render(request, "store/create_store.html", {"form":form})


@login_required(login_url="/login/")
def edit_view(request, store_id):
    store = Store.objects.get(id=store_id)

    if request.method == "POST":
        form = StoreForm(request.POST, instance=store)
        if form.is_valid():
            form.save()
            return redirect("index")
        else:
            return render(request, "store/edit_store.html", {"error":form.errors})
    else:
        form = StoreForm(instance=store)
        return render(request, "store/edit_store.html", {"form":form})


@login_required(login_url="/login/")
def list_my_stores(request):
    stores = Store.objects.filter(owner=request.user)
    return render(request, "store/my_stores.html", {"stores":stores})


@login_required(login_url="/login/")
def delete_view(request, store_id):
    store = Store.objects.get(id=store_id)
    store.delete()
    return redirect("index")