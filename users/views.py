from .forms import SignupForm
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect

from products.models import Product


def signup(request):
   
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            new_user.set_password(new_user.password)
            new_user.username = new_user.email ################
            new_user.save()
            login(request, new_user)
            return redirect('index')
    else:
        form = SignupForm()
    return render (request,'registration/signup.html',{'form':form})


def login_view(request):
    next_page = request.GET['next']

    if request.method == 'GET':
        context = ''
        return render(request, 'registration/login.html', {'context': context})

    elif request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(next_page)
        else:
            return render(request, 'registration/login.html', {'error': 'Wrong credentials'})

# def signup_view(request):

#     form = SignupForm(request.POST)

#     if request.method == 'POST':
#         if form.is_valid():
#             form.save()
#             return render (request,'index',{'products':Product.objects.all()})
#     else:
#         form = SignupForm()
#     return render (request,'registration/signup.html',{'form':form})