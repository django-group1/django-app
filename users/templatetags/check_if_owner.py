from django import template

register = template.Library()
@register.filter
def is_owner(user):
    return user.is_owner