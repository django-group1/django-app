from django import template
from django.shortcuts import render


register = template.Library()
@register.filter
def check_is_owner(view_func):
    def wrap(request,**kwargs):
        user = request.user
        if user.is_owner:
            return view_func(request,**kwargs)
        else:
            return render(request,"base/404.html")
    return wrap